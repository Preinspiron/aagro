import { defineConfig } from 'astro/config';
import preact from '@astrojs/preact';
// import svgr from "@svgr/rollup";

// https://astro.build/config
export default defineConfig({
    root: '.',
    srcDir: 'src',
    publicDir: 'src/public',
    i18n: {
        defaultLocale: 'ua',
        locales: ['en', 'ua', 'tr'],
        routing: {
            prefixDefaultLocale: false,
        },
    },
    vite: {
        // plugins: [svgr()],
    },
    integrations: [preact({ compat: true })],
});
