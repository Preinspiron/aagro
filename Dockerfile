FROM alpine
LABEL maintainer = 'EDICOM'
COPY --chown=node:node package*.json ./
RUN apk add --no-cache bash && \
    apk add --update nodejs npm && \ 
    apk add --no-cache chromium
RUN npm install -g pnpm
RUN npm install -g lighthouse
RUN npm install -g @lhci/cli@
RUN pnpm install
