import { useTranslations } from "@i18n/utils";
import { getLangFromUrl } from "@i18n/utils_clinet";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import { useCallback, useEffect, useMemo, useState } from "preact/hooks";
import {
  GeoJSON,
  MapContainer,
  Marker,
  Popup,
  useMap,
  useMapEvent,
} from "react-leaflet";
import geojson from "./map.json";
import s from "./propos.module.scss";
import "./propos.scss";
import { fetchGoogleSheetData } from "./dataModel";

import { SHAPEISO } from "./shape-iso.js";
import { SELECT_DATA } from "./selectData";
import { customIcon } from "./iconPoint";
import RangeSliderWithButtons from "./RangeSliderWithButtons";
import Bank from "./Bank";

const lang = getLangFromUrl(window.location);
const t = useTranslations(lang);

export const Map = () => {
  const [bankFilter, setBankFilter] = useState([0, 20000]);
  const [regionFilter, setRegionFilter] = useState("");
  const [typeFilter, setTypeFilter] = useState("");
  const [popupData, setPopupData] = useState(null);
  const [popupPosition, setPopupPosition] = useState(null);
  const [zoomLevel, setZoomLevel] = useState(4); // Начальный уровень зума

  // data google sheets
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const loadData = async () => {
      try {
        const data = await fetchGoogleSheetData();
        console.log("Google Sheets Data:", data);

        if (Array.isArray(data) && data.length > 0) {
          // Преобразуем ключи объектов (если нужно)
          const formattedData = data.map((item) => ({
            bank: item.bank || "",
            price: item.price || "",
            regionUa: item.regionUa || "",
            type: item.type || "",
            timestamp: item["Отметка времени"] || "", // Переименовываем ключ
          }));

          setItems(formattedData);
        }
      } catch (err) {
        setError(err.message || "Unknown error");
      } finally {
        setLoading(false);
      }
    };

    loadData();
  }, []);

  const MapComponent = ({ setZoomLevel }) => {
    const map = useMap();

    useMapEvent({
      zoomend: (e) => {
        const newZoomLevel = e.target.getZoom();
        setZoomLevel(newZoomLevel);
        // Обновляем подписи или стили в зависимости от нового уровня зума
        map.eachLayer((layer) => {
          if (layer instanceof L.GeoJSON) {
            layer.eachLayer((subLayer) => {
              const { feature } = subLayer;
              if (feature.properties && feature.properties.shapeName) {
                if (newZoomLevel >= 4) {
                  subLayer
                    .bindTooltip(
                      t("map.region." + feature.properties.shapeISO),
                      {
                        permanent: true,
                        direction: "center",
                        className: s["leaflet-label"],
                        offset: [0, 0],
                      },
                    )
                    .openTooltip();
                } else {
                  subLayer.unbindTooltip();
                }
              }
            });
          }
        });
      },
      click: (e) => {
        console.log("Map clicked at: ", e.latlng);
      },
    });

    return null;
  };

  const transformData = (items) => {
    return items.map((item, index) => {
      const regionData = SHAPEISO.find(
        (region) => region.name === item.regionUa,
      );

      return {
        id: item.id || index,
        regionUa: item.regionUa,
        region: item.region || (regionData && regionData.region),
        shapeISO: regionData.shapeISO,
        type: Array.isArray(item.type)
          ? item.type
          : item.type?.split(", ").map((t) => t.trim()) || [],
        price: Number(item.price) || 0,
        bank: Number(item.bank) || 0,
        leaseRent: item["lease-rent"] || "",
        description: item.description || "",
      };
    });
  };

  const transformedItems = useMemo(() => transformData(items), [items]);

  const filteredData = useMemo(() => {
    return transformedItems.filter((item) => {
      // console.log("item", item);

      return (
        item.bank >= bankFilter[0] &&
        item.bank <= bankFilter[1] &&
        (regionFilter === "" || item.shapeISO.trim() === regionFilter) &&
        (typeFilter.length === 0 ||
          item.type.some((t) => typeFilter.includes(t)))
      );
    });
  }, [bankFilter, regionFilter, typeFilter, transformedItems]);

  console.log("filteredData", filteredData);

  useEffect(() => {}, [zoomLevel]);

  const onEachFeature = (feature, layer) => {
    const regionName = t("map.region." + feature.properties.shapeISO);
    if (feature.properties && feature.properties.shapeName) {
      layer
        .bindTooltip(regionName, {
          permanent: true,
          direction: "center",
          className: s["leaflet-label"],
          offset: [0, 0],
        })
        .openTooltip();
    }

    layer.on({
      click: (e) => {
        console.log("filterdata", filteredData);

        console.log(
          "feature.properties.shapeISO on this",
          feature.properties.shapeISO,
        );
        const objectsInRegion = filteredData.filter(
          (item) => item.shapeISO === feature.properties.shapeISO,
        );

        setPopupData({
          regionName: regionName,
          objects: objectsInRegion,
        });

        setPopupPosition(e.latlng);

        console.log("regionName", regionName);
        console.log("popupData", popupData, "objectsInRegion", objectsInRegion);
      },
      mouseover: (e) => {
        const layer = e.target;
        layer.setStyle(highlightStyle);
        layer.bringToFront();
      },
      mouseout: (e) => {
        const layer = e.target;
        layer.setStyle(defaultStyle);
      },
    });
  };

  const calculateCenter = (feature) => {
    const bounds = L.geoJSON(feature).getBounds();
    // console.log(bounds);
    switch (feature.properties.shapeISO) {
      case "UA-51":
        const center = bounds.getCenter();
        const adjustedCenter = L.latLng(center.lat + 0.12, center.lng + 0.7);
        return adjustedCenter;
    }
    return bounds.getCenter();
  };
  const regionCenters = geojson.features.reduce((acc, feature) => {
    const center = calculateCenter(feature);
    const regionId = feature.properties.shapeISO; // Предположим, что идентификатор региона это shapeISO
    acc[regionId] = [center.lat, center.lng];
    return acc;
  }, {});

  const defaultStyle = {
    color: "#FFFFFF", // Обводка белым цветом
    fillColor: "#efece5", // Заливка серым цветом
    weight: 2,
    opacity: 1,
    fillOpacity: 1,
  };

  const highlightStyle = {
    color: "#FFFFFF", // Обводка белым цветом при наведении
    fillColor: "#F2DBB1", // Заливка другим цветом при наведении
    weight: 2,
    opacity: 0.8,
    fillOpacity: 0.8,
  };

  // __________________________________________________________________________________________________________

  return (
    <div class={s["map__selector-wrapper"]}>
      <div className="map__selector">
        <select
          class="map__selector-region"
          onChange={(e) => setRegionFilter(e.target.value)}
          value={regionFilter}
        >
          <option value="">{t("map.choice")}</option>
          {Array.from(new Set(filteredData.map((item) => item.shapeISO))).map(
            (region) => (
              <option key={region} value={region}>
                {t("map.region." + region) ?? region}
              </option>
            ),
          )}
        </select>

        <RangeSliderWithButtons
          bankFilter={bankFilter}
          setBankFilter={setBankFilter}
        ></RangeSliderWithButtons>

        <div class="map__selector-type">
          {SELECT_DATA.map((type) => (
            <label key={type} className="map__selector-type-label">
              <input
                type="radio"
                name="type"
                onChange={(e) => setTypeFilter(e.target.value)}
                value={type}
              />
              <span>{t("map.type." + type) ?? type}</span>
            </label>
          ))}
          <label class="map__selector-type-label">
            <input
              checked={typeFilter.length === 0}
              type="radio"
              name={"type"}
              id="type"
              onChange={(e) => setTypeFilter(e.target.value)}
              value=""
            />
            <span>{t("all.type.choice")}</span>
          </label>{" "}
        </div>
        <Bank transformedItems={transformedItems} />
      </div>

      {loading && (
        <div class="loading-overlay">
          <div class="spinner"></div>
        </div>
      )}

      {!loading && (
        <div class={s.leaflet__wrapper}>
          <MapContainer
            center={
              matchMedia("(min-width:1024px)").matches
                ? [48.447055, 31.2]
                : [48.447055, 31.2]
            }
            zoom={matchMedia("(min-width:1024px)").matches ? 6 : 4.5}
            zoomAnimation={true} // Включить анимацию
            zoomAnimationThreshold={4} //
            zoomSnap={0.1} // шаг масштабирования
            zoomDelta={0.25} // настройка чувствительности зума
            scrollWheelZoom={false}
            zoomControl={true}
            attributionControl={false}
            style={{ height: "100%", width: "100%" }}
          >
            <GeoJSON
              data={geojson.features}
              onEachFeature={onEachFeature}
              style={defaultStyle}
            />
            {popupData && (
              <Popup position={popupPosition}>
                <div>
                  <h4 class={s["map-modal__title"]}>{popupData.regionName}</h4>
                  <ul class={s["map-modal"]}>
                    {console.log("popupData", popupData)}
                    {popupData.objects.length > 0 ? (
                      popupData.objects.map((item) => (
                        <lo class={s["map-modal__item"]} key={item.id}>
                          <div>
                            {t("map.land.bank")}{" "}
                            {Intl.NumberFormat("ru-RU").format(item.bank)}{" "}
                            {t("hectares")}
                          </div>

                          {item.price === 0 ? (
                            <div></div>
                          ) : (
                            <div>
                              {t("map.object")}
                              {Intl.NumberFormat("ru-RU", {
                                style: "currency",
                                currency: "USD",
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 0,
                              }).format(item.price)}{" "}
                            </div>
                          )}
                        </lo>
                      ))
                    ) : (
                      <li>{t("no.object")}</li>
                    )}
                  </ul>
                  {popupData.objects.length > 0 && (
                    <a
                      class="map-modal__tel"
                      href="tel:+380677937790"
                      style={{
                        marginTop: "10px",
                        color: "#8d0200",
                        textDecoration: "none",
                      }}
                    >
                      +380677937790
                    </a>
                  )}
                </div>
              </Popup>
            )}

            {Object.keys(regionCenters).map((region) => {
              // Проверка наличия объектов в регионе. маркер на регионе
              const hasObjects = filteredData.some(
                (item) => item.shapeISO === region,
              );
              return (
                hasObjects && (
                  <Marker
                    icon={customIcon}
                    key={region}
                    position={regionCenters[region]}
                  >
                    <Popup>{t("map.region." + region)}</Popup>
                  </Marker>
                )
              );
            })}
            <MapComponent setZoomLevel={setZoomLevel} />
          </MapContainer>
        </div>
      )}
    </div>
  );
};
