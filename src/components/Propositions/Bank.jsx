import React, { useState, useEffect, useRef } from "react";
import "./propos.scss";
import { useTranslations } from "@i18n/utils";
import { getLangFromUrl } from "@i18n/utils_clinet";

const Bank = ({ transformedItems }) => {
  const [totalBank, setTotalBank] = useState(0);
  const [count, setCount] = useState(0);
  const [hasAnimated, setHasAnimated] = useState(false);
  const elementRef = useRef(null);

  const lang = getLangFromUrl(window.location);
  const t = useTranslations(lang);

  useEffect(() => {
    const total = transformedItems
      ? transformedItems.reduce((acc, item) => acc + (item.bank || 0), 0)
      : 0;
    setTotalBank(total);
  }, [transformedItems]);

  useEffect(() => {
    if (hasAnimated || totalBank === 0) return;

    const observer = new IntersectionObserver(
      ([entry]) => {
        if (entry.isIntersecting) {
          setHasAnimated(true);

          setTimeout(() => {
            // Добавляем задержку в 1 секунду
            let startTime = null;
            const duration = 1000;

            const step = (timestamp) => {
              if (!startTime) startTime = timestamp;
              const progress = timestamp - startTime;
              const increment = Math.min(
                Math.floor((progress / duration) * totalBank),
                totalBank,
              );

              setCount(increment);

              if (progress < duration) {
                requestAnimationFrame(step);
              }
            };

            requestAnimationFrame(step);
          }, 1000);
        }
      },
      { threshold: 0.5 },
    );

    if (elementRef.current) {
      observer.observe(elementRef.current);
    }

    return () => {
      if (elementRef.current) {
        observer.unobserve(elementRef.current);
      }
    };
  }, [totalBank, hasAnimated]);

  return (
    <div ref={elementRef} className="bank-container">
      <h2 className="title-bank">{t("bank-text")}:</h2>
      <p className="data-bank">
        <span className="data-bank-red">{count}</span> {t("hectares")}
      </p>
    </div>
  );
};

export default Bank;
