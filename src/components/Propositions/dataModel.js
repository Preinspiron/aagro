import PublicGoogleSheetsParser from "public-google-sheets-parser";

export const fetchGoogleSheetData = async () => {
  const options = { sheetName: "Sheet1", useFormat: true };
  try {
    const parser = new PublicGoogleSheetsParser(
      "1mh6w98lltgu3vgDxbo_tsUfNoocqkb42UjjNv8dXAeI",
      options,
    );
    return await parser.parse();
  } catch (error) {
    console.error("Error fetching Google Sheets data:", error);
    throw error;
  }
};
