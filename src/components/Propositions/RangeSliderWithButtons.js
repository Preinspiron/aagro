import React, { useState } from "react";
import { Range, getTrackBackground } from "react-range";
import "./propos.scss";

import { useTranslations } from "@i18n/utils";
import { getLangFromUrl } from "@i18n/utils_clinet";

const lang = getLangFromUrl(window.location);
const t = useTranslations(lang);

const RangeSliderWithButtons = ({ bankFilter, setBankFilter }) => {
  const min = 0;
  const max = 20000;
  const step = 1000;

  const handleRangeChange = (values) => {
    setBankFilter(values);
  };

  return (
    <div className="map__selector-type">
      <label className="map__selector-range">
        {/* <div className="range-buttons-box">
          <button
            className="range-button"
            onClick={() =>
              setBankFilter([Math.max(bankFilter[0] - 1000, 0), bankFilter[1]])
            }
          >
            <svg width="36" height="36" className="icon icon-left">
              <use href="/sprite.svg#arrow"></use>
            </svg>
          </button>
          <button
            className="range-button"
            onClick={() =>
              setBankFilter([
                Math.min(bankFilter[0] + 1000, 20000),
                bankFilter[1],
              ])
            }
          >
            <svg width="36" height="36" className="icon">
              <use href="/sprite.svg#arrow"></use>
            </svg>
          </button>
        </div> */}

        <Range
          values={bankFilter}
          step={step}
          min={min}
          max={max}
          onChange={handleRangeChange}
          renderTrack={({ props, children }) => (
            <div
              {...props}
              style={{
                ...props.style,
                height: "5px",
                width: "100%",
                borderRadius: "4px",
                background: getTrackBackground({
                  values: bankFilter,
                  colors: ["#ccc", "#e00502", "#ccc"],
                  min,
                  max,
                }),
                alignSelf: "center",
              }}
            >
              {children}
            </div>
          )}
          renderThumb={({ props }) => (
            <div
              {...props}
              style={{
                ...props.style,
                height: "20px",
                width: "20px",
                borderRadius: "50%",
                backgroundColor: "#FFF",
                boxShadow: "0px 2px 6px #AAA",
              }}
            />
          )}
        />

        <span>
          {t("from")} {bankFilter[0]} {t("to")} {bankFilter[1]} {t("hectares")}
        </span>
      </label>
    </div>
  );
};

export default RangeSliderWithButtons;
