const iconPoint = `
    <svg xmlns='http://www.w3.org/2000/svg' fill='none'>
        <path
            fill='#e00502'
            style='fill: var(--color1, #e00502)'
            d='M8.732 0.264c-6.981-0.020-8.738 5.227-8.732 8.327 0.013 6.685 3.567 11.803 8.816 20.146 5.129-8.432 8.584-13.448 8.584-20.121 0-3.294-1.687-8.332-8.668-8.352zM8.648 4.485c2.401-0 4.347 1.874 4.347 4.186s-1.947 4.186-4.347 4.186c-2.4-0-4.346-1.874-4.346-4.186s1.946-4.185 4.346-4.186z'
        ></path>
    </svg>`;

// Преобразование SVG в URL
const svgUrl = "data:image/svg+xml;base64," + btoa(iconPoint);

// Определение кастомной иконки
export const customIcon = new L.Icon({
  iconUrl: svgUrl,
  iconSize: [32, 32], // Размер иконки
  iconAnchor: [16, 32], // Якорь иконки
  popupAnchor: [0, -32], // Смещение попапа
});
