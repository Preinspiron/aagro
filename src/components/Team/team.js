import Splide from "@splidejs/splide";
import { AutoScroll } from "@splidejs/splide-extension-auto-scroll";

document.addEventListener("DOMContentLoaded", function () {
  var splide = new Splide(".team", {
    perPage: 3,
    gap: "3rem",
    type: "loop",

    breakpoints: {
      768: {
        perPage: 1,
        autoplay: true,
        interval: 3000,
        perMove: 1,
        focus: "center",
      },
      999: {
        perPage: 2,
      },
    },
    pagination: false,
    arrows: true,
    autoplay: false,
  });

  splide.mount();
});

const splide2 = new Splide(".partners__wrapper", {
  arrows: false,
  // padding: 16,
  autoHeight: true,
  autoWidth: true,
  fixedWidth: 200,
  perPage: 4,
  pagination: false,
  mediaQuery: "min",
  breakpoints: {
    1024: {
      // width: '100%',
      gap: 150,
      // type: 'loop',
      type: "loop",
      drag: "free",
      focus: "center",
      autoScroll: {
        speed: 1,
      },
    },

    320: {
      gap: 16,

      type: "loop",
      drag: "free",
      focus: "center",
      autoScroll: {
        speed: 1,
      },
    },
  },
}).mount({ AutoScroll });
