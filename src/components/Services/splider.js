import { gsap } from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

gsap.utils.toArray(".services__service-item").forEach((item, index) => {
  gsap.from(item, {
    scrollTrigger: {
      trigger: item,
      start: "top 90%", // Начинает анимацию, когда элемент на 80% в зоне видимости
      toggleActions: "play none none none", // Анимация срабатывает только один раз
    },
    opacity: 0,
    y: 50,
    duration: 1,
    ease: "power2.out",
    delay: index * 0.1, // Добавляет небольшую задержку между элементами
  });
});
