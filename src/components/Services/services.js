import { gsap } from "gsap";

import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

let mm = gsap.matchMedia();
const list = gsap.utils.toArray(".import__item");

const img = gsap.utils.toArray(".image__wrapper img");
img.forEach((item, id) => {
  item.gsapAnimation = gsap.to(item, {
    opacity: 1,
    zIndex: id + 1,
    // stagger: 1,
  });
});

mm.add("(min-width: 1024px)", () => {
  list.forEach((item, id) => {
    ScrollTrigger.create({
      // markers: true,
      trigger: item,

      toggleClass: "isActive",
      animation: img[id]?.gsapAnimation,
      //   start: () => "top center",
      //   end: () => "center+=150 center-=150",
      start: () => "top 90%",
      end: () => "center+=100 center-=100",
      scrub: 0.5,
    });
  });
  return () => {};
}).add("(max-width: 1023px)", () => {
  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: ".import__list",
      pin: true,
      anticipatePin: 1,
      //   markers: true,
      start: "top top",
      end: "+=300%",
      scrub: true,
      snap: [0, 0.37, 0.66, 0.93],
      pinSpacer: false,
    },
  });

  mm.add("(max-width: 1023px)", () => {
    // Анимации для мобильных
    list.forEach((item, id) => {
      if (id === 0) {
        tl.fromTo(item, { opacity: 1 }, { opacity: 0, zIndex: 0 });
      } else {
        tl.fromTo(
          item,
          { zIndex: 0, opacity: 0 },
          { opacity: 1, zIndex: id + 1 },
        ).add(tl.to(item, { opacity: 0, duration: 0.1 }), "<+=2%");
      }
    });
  });

  // Анимации для десктопа

  mm.add("(min-width: 1024px)", () => {
    list.forEach((item, id) => {
      if (id === 0) {
        tl.fromTo(item, { opacity: 1 }, { opacity: 0, zIndex: 0 });
      } else {
        tl.fromTo(
          item,
          { zIndex: 0, opacity: 0 },
          { opacity: 1, zIndex: id + 1 },
        ).add(tl.to(item, { opacity: 0, duration: 1 }), "<+=1%");
      }
    });
  });
  return () => {};
});

// tl.fromTo(item, { zIndex: 0, opacity: 0 }, { opacity: 1, zIndex: id + 1 }).add(
//   tl.to(item, { opacity: 0, duration: 0.1 }),
//   "<+=2%",
// );
