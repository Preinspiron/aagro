import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
gsap.registerPlugin(ScrollTrigger);

const REFS = {
    section: document.querySelector('#benefits'),
    items: gsap.utils.toArray('.benefits__item'),
};

gsap.registerEffect({
    name: 'fade',
    effect: (targets, config) => {
        return gsap.fromTo(
            targets,
            {
                duration: config.duration,
                opacity: 0,
                xPercent: -50,
            },
            {
                duration: config.duration,
                opacity: 1,
                xPercent: 0,

                stagger: 0.3,
            }
        );
    },
    defaults: { duration: 2 },
    extendTimeline: true,
});

ScrollTrigger.create({
    trigger: REFS.section,
    start: 'top center',
    end: 'top center',
    animation: gsap.effects.fade('.benefits__item'),
});
