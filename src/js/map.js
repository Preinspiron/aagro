import { isArray } from "@splidejs/splide/src/js/utils";
import { objects } from "./data.json";

const point = `
<path fill="#e00502" style="fill: var(--color1, #e00502)" d="M8.732 0.264c-6.981-0.020-8.738 5.227-8.732 8.327 0.013 6.685 3.567 11.803 8.816 20.146 5.129-8.432 8.584-13.448 8.584-20.121 0-3.294-1.687-8.332-8.668-8.352zM8.648 4.485c2.401-0 4.347 1.874 4.347 4.186s-1.947 4.186-4.347 4.186c-2.4-0-4.346-1.874-4.346-4.186s1.946-4.185 4.346-4.186z"></path>
`;

const checkbox = document.querySelectorAll("#type");
const selectRegion = document.querySelector(".map__selector-region");
const mapRegion = document.querySelectorAll("[id*='UA']");
const backdrop = document.querySelector(".backdrop");
const btnCloseModal = document.querySelector("[data-modal='map']");
const modal = document.querySelector(".map__propositions");
const modalTitle = document.querySelector(".map-propositions__title");

const set = new Set();

objects.forEach((item, index) => {
  if (set.has(item.mapRegion)) return;
  set.add(item.mapRegion);
  const newRegion = document.createElement("option");
  newRegion.textContent = item.regionUa;
  newRegion.value = item.mapRegion;
  selectRegion.appendChild(newRegion);
});

mapRegion.forEach((region, index) => {
  region.classList.add("not__marker__region");
  for (const mapRegion in objects) {
    if (region.id === objects[mapRegion].mapRegion) {
      renderMarker(region);
    }
  }
});

const map = document.querySelector("#map");

const selected = [];

function objectsChecker(item) {
  return objects.filter((t) => t.mapRegion === item.id);
}

function modalRender(arr) {
  const modalList = document.querySelector(".map-propositions__list");

  if (arr.length === 0) return (modalList.innerHTML = "no items");
  modalList.textContent = "";
  modalTitle.textContent = arr[0].regionUa;
  arr.forEach((item, idx) => {
    const obj = document.createElement("li");

    const objPrice = document.createElement("p");
    const objBank = document.createElement("p");
    const objType = document.createElement("p");

    objPrice.textContent = "Ціна: " + item.price.toLocaleString("ru-RU") + " $";
    objBank.textContent =
      "Земельний банк: " + item.bank.toLocaleString("ru-RU") + " Га";
    objType.innerHTML = isArray(item.typeUa)
      ? "Цільове призначення: " +
        item.typeUa
          .map((item, idx) => `<span id='${idx}'>${item}</span>`)
          .join("")
      : "no type";
    obj.append(objBank, objPrice, objType);
    modalList.appendChild(obj);
  });
}

map.addEventListener("click", (e) => {
  modal.classList.remove("ishidden");
  const target = e.target;
  const parent = target.closest("[id]");
  if (parent) {
    const items = objectsChecker(parent);
    parent.classList.contains("marked__region") && modalRender(items);

    mapRegion.forEach((region) => {
      region.classList.remove("selected");
      region === parent && parent.classList.toggle("selected");
      backdrop.classList.toggle("ishidden");
      document.body.style.overflow = "hidden";
    });
    selected.push(parent);
  }
});

const setSelect = new Set();

checkbox.forEach((select, index) => {
  const mapRegions = document.querySelectorAll(".marked");
  select.addEventListener("change", () => {
    mapRegion.forEach((item) => item.classList.remove("selected"));
    if (select.name === "all" && select.checked) {
      checkbox.forEach((item) => (item.checked = true));
      setSelect.clear();
      setSelect.add(select.name);
    } else if (select.name === "all" && select.checked === false) {
      checkbox.forEach((item) => (item.checked = false));
      setSelect.clear();
    }

    if (select.checked) {
      setSelect.add(select.name);
      select.checked = true;
      mapRegions.forEach((item) => item.remove());
    } else {
      setSelect.delete(select.name);
      select.checked = false;
    }
    updateMarkers();
  });
});

const rangeInput = document.getElementById("rangeInput");
const tooltip = document.getElementById("tooltip");
rangeInput.addEventListener("input", (e) => {
  updateTooltip(e.target.value);
});

const findObjectsFromSet = (set, obj) =>
  obj.filter((item) => item.type.some((t) => set.has(t)));

function updateMarkers() {
  const mapRegions = document.querySelectorAll(".marked");
  mapRegion.forEach((item) => item.classList.remove("marked__region"));
  mapRegions.forEach((item) => item.remove());
  if (setSelect.has("all"))
    return mapRegion.forEach(
      (region) =>
        objects.some((t) => t.mapRegion === region.id) && renderMarker(region)
    );
  if (setSelect.size === 0)
    return mapRegion.forEach((region) => {
      mapRegions.forEach((item) => item.remove());
      // region.classList.remove("selected");
    });
  const obj = findObjectsFromSet(setSelect, objects);
  mapRegion.forEach((region) => {
    region.classList.remove("marked__region");
    if (obj.some((t) => t.mapRegion === region.id)) {
      // region.classList.add("marked__region");
      renderMarker(region);
    }
  });
}

function updateTooltip(value) {
  tooltip.innerText = value;
  tooltip.style.left = `${
    ((value - rangeInput.min) / (rangeInput.max - rangeInput.min)) * 100
  }%`;
}
function renderMarker(region) {
  region.classList.add("marked__region");
  region.classList.remove("not__marker__region");
  const bbox = region.getBBox();
  const centerX = bbox.x + bbox.width / 2 - 8;
  const centerY = bbox.y + bbox.height / 2 - 14;
  const marker = document.createElementNS("http://www.w3.org/2000/svg", "g");
  marker.innerHTML = point;
  marker.setAttribute("transform", `translate(${centerX}, ${centerY})`);
  marker.classList.add("marked");
  region.appendChild(marker);
}

btnCloseModal.addEventListener("click", () => {
  const modalList = document.querySelector(".map-propositions__list");
  backdrop.classList.toggle("ishidden");
  modal.classList.add("ishidden");
  document.body.style.overflow = "auto";
  modalTitle.textContent = "";
  modalList.textContent = "no items in this Region";
});
