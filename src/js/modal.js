const REFS = {
  btn: document.querySelector("[data-btnHeroModal"),
  modal: document.querySelector("[data-heroModal]"),
  backdrop: document.querySelector(".backdrop"),
  btnCloseModal: document.querySelector("[data-modal='hero"),
};

const ACTIONS = {
  openModal: () => {
    REFS.backdrop.classList.toggle("ishidden");
    REFS.modal.classList.toggle("ishidden");
  },
  closeModal: () => {
    REFS.backdrop.classList.toggle("ishidden");
    REFS.modal.classList.toggle("ishidden");
  },
};

console.log(ACTIONS.openModal);
console.log(ACTIONS.closeModal);
REFS.btn.addEventListener("click", ACTIONS.openModal);
REFS.btnCloseModal.addEventListener("click", ACTIONS.closeModal);

export {};
