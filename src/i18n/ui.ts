import ua from './ua.json';
import en from './en.json';
import tr from './tr.json';
export const showDefaultLang = false;

export const languages = {
    ua: 'Ukraine  🇺🇦',
    en: 'English 🇺🇸',
    tr: 'Turkey 🇹🇷',
};

export const defaultLang = 'ua';

export const ui = {
    ua,
    en,
    tr,
} as const;
