import { ui, defaultLang, showDefaultLang } from './ui';
export function getLangFromUrl() {
    const pathSegments = window.location.pathname.split('/');
    const lang = pathSegments[1]; // Предполагается, что язык находится на первом уровне в URL

    if (lang in ui) {
        return lang as keyof typeof ui;
    }

    return defaultLang;
}
